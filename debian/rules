#!/usr/bin/make -f
# See debhelper(7) (uncomment to enable)
# output every command that modifies files on the build system.
#export DH_VERBOSE = 1

export PYBUILD_NAME=pymatgen

%:
	dh $@ --with python3,sphinxdoc --buildsystem=pybuild

override_dh_auto_build-indep: export http_proxy=127.0.0.1:9
override_dh_auto_build-indep: export https_proxy=127.0.0.1:9
override_dh_auto_build-indep:
	dh_auto_build
	PYTHONPATH=$(shell pybuild --pyver `py3versions --default -v` --print build_dir | awk '{print $$3}' ) \
	  make -C docs_rst html BUILDDIR=$(CURDIR)/build/html

override_dh_auto_test:
	echo "skipping tests until the pymatgen-test-files package becomes available"

skip_override_dh_auto_test:
	py=`py3versions --default -v`; \
	pybuilddir=`pybuild --pyver $$py --print build_dir | awk '{print $$3}'`; \
	testdir=$(CURDIR)/.pybuild/test_python$$py; \
	cp -a $$pybuilddir $$testdir; \
	for t in `find pymatgen -name tests`; do \
	  d=`dirname $$t`; \
	  cp -a $$t $$testdir/$$d; \
	done; \
	PMG_TEST_FILES_DIR=/usr/share/doc/pymatgen-test-files/examples/test_files PYTHONPATH=$$testdir pytest-3 -v --color=no $$testdir; \

override_dh_installdocs-indep:
	dh_installdocs -i
	# use multiline sed to remove entire div block for google-analytics.com
	grep "google-analytics.com" debian/python-pymatgen-doc/usr/share/doc/python-pymatgen-doc/html/* -r --files-with-matches | xargs sed '/./{H;$$!d} ; x ; s/<div class="footer">.*google-analytics.com.*<\/div>//' -i
	grep 'src="https://anaconda.org/conda-forge/pymatgen/badges/downloads.svg"' debian/python-pymatgen-doc/usr/share/doc/python-pymatgen-doc/html/* -r --files-with-matches | xargs sed 's|src="https://anaconda.org/conda-forge/pymatgen/badges/downloads.svg"|src="_images/downloads.svg"|' -i
	grep 'src="https://github.com/materialsproject/pymatgen/actions/workflows/test.yml/badge.svg"' debian/python-pymatgen-doc/usr/share/doc/python-pymatgen-doc/html/* -r --files-with-matches | xargs sed 's|src="https://github.com/materialsproject/pymatgen/actions/workflows/test.yml/badge.svg"|src="_images/tests_passing.svg"|' -i
	grep 'src="https://coveralls.io/repos/github/materialsproject/pymatgen/badge.svg?branch=master"' debian/python-pymatgen-doc/usr/share/doc/python-pymatgen-doc/html/* -r --files-with-matches | xargs sed 's|src="https://coveralls.io/repos/github/materialsproject/pymatgen/badge.svg?branch=master"|src="_images/coverage.svg"|' -i
	grep 'src="https://cdnjs.cloudflare.com/ajax/libs/octicons/.*/svg/mark-github.svg"' debian/python-pymatgen-doc/usr/share/doc/python-pymatgen-doc/html/* -r --files-with-matches | xargs sed 's|src="https://cdnjs.cloudflare.com/ajax/libs/octicons/.*/svg/mark-github.svg"|src="_images/mark-notgithub.svg"|' -i
